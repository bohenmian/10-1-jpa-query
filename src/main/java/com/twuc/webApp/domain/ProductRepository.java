package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    List<Product> findByProductLineIn(List<ProductLine> productLines);

    List<Product> findByQuantityInStockBetween(Short start, Short end);

    List<Product> findByQuantityInStockBetweenOrderByProductCode(Short start, Short end);

    Page<Product> findByQuantityInStockBetweenOrderByProductCode(Pageable pageable, Short start, Short end);

    // --end-->
}
